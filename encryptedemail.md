** How to use PGP encryption for Gmail in under 10 minutes **

### Step 1: Download the Chrome Browser ###
* If you already have the Google Chrome browser installed, skip this step. 
* One of the features Google claims when marketing Chrome is, that it “is designed to be fast in every possible way. It’s quick to start up from your desktop, loads web pages in a snap, and runs complex web applications lightning fast
* Though Gmail is mentioned as an example in this tutorial, the procedure can be used for other mail providers like Yahoo, Live etc. You can download Chrome here: https://www.google.com/chrome/browser/features.html

### Step 2: Download Mailvelope for Chrome extension ###
* Just like communication via phone or email, users need compatible equipment on both sides, with PGP both parties need to have some sort of encryption/decryption software installed
* Go to Mailvelope and download Mailvelope for Chrome extension (https://chrome.google.com/webstore/detail/mailvelope/kajibbejlbohfaggdiogboambcijhkke?utm_source=chrome-ntp-icon)
* You should be able to see the Mailvelope’s padlock icon on the top right of your browser after the installation.

### Step 3: Generate public and private keys for your account ###
* Like an email address, your public key is the address you give others to send you encrypted email that only you can read. Your private key is your password to unlock, send (encrypt) and read (decrypt) messages sent to your account.
* Click on the Mailvelope icon, choose More Options and click on Manage Keys, Generate Key. Fill out the displayed form and submit it by filling in your name, email address and create a password. (This password is not your Gmail password but one you create to let you send and receive encrypted messages). Click on the Display Keys to ensure that your name and email address are added to the Key Ring list
* Note: You can leave the Advanced tab containing algorithm, key size and expiry date at default values or click on the tab to specify them

### Step 4: To send email ###
* To send someone encrypted email, it is important that the person you wish to send messages to has shared his or her public key address with you (it is the long string of letters and numerals representing the address of the person you wish to send email to). Many PGP users store their public keys on public key servers. MIT PGP Public Key Serveris one of these servers: https://pgp.mit.edu/
* For this assignment, use the key ***'Nishant_cpsc420620 (cpsc420620@gmail.com)'. Also make sure that you pick the right public key when you send the email.***.
* Click on Import Keys on the menu on the left. Paste the public key of the account to which you want to send email and press Submit. When “Display Keys” is pressed, the Key Ring list now contains the recipient’s public key.
* Now, open your Gmail and click on Compose. As usual, type in the email address, subject and the message. Notice the Mailvelope compose icon in the typing area
* Click on the icon to bring up the Compose Mail window. Click on the Padlock icon and choose the recipient’s email address from the list and HTML/plain text option. Press OK.
* The message is now encrypted. Click on Transfer. Press Send.

### Step 5: To receive email. ###
* Before someone can send you an encrypted message, you need to share your public key with that person. To find your public key, click on Display Keys. Choose your email address from the Key Ring list and click Export. Choose Display Public Key. Copy/paste the long string of characters and numerals and share it with the person you want to receive messages from.
* If you want to upload it to a public server so anyone can find it, download the file and go to the MIT Public Key Server (https://pgp.mit.edu/”); here you can upload your public key.
* If someone sends you an encrypted email message in your GMail Inbox, you will see a message encrypted by letters and numerals. To make sense of it, click on the envelope icon on the message. Enter your password to unlock the message and read it.
* When you have more than 10 minutes, you should definitely spend some time learning more about PGP. This guide was meant to be a quick start, but there is a lot more you will need to learn to ensure absolute privacy. If you don’t think it is necessary to exercise your right to privacy, please watch the below video.
